package main

import (
	"fmt"
	"os"

	"./title1"
)

func main() {
	err := title1.Title(os.Args[1])
	if err != nil {
		fmt.Printf("%v", err)
	}
}
