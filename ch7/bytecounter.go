package main

import (
	"fmt"

	"./bytecounter"
)

func main() {
	var c bytecounter.ByteCounter

	c.Write([]byte("Hello"))
	fmt.Println(c)

	// c = 0
	var name = "Dolly"
	fmt.Fprintf(&c, "hello, %s", name)
	fmt.Println(c)
}
