package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	db := database{"shoes": 50, "socks": 5}
	// log.Fatal(http.ListenAndServe("localhost:8000", db))
	// mux := http.NewServeMux()
	// mux.Handle("/list", http.HandlerFunc(db.list))
	// mux.Handle("/price", http.HandlerFunc(db.price))
	http.HandleFunc("/list", db.list)
	http.HandleFunc("/price", db.price)
	// log.Fatal(http.ListenAndServe("localhost:8000", mux))
	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}

type dollar float32

func (d dollar) String() string {
	return fmt.Sprintf("$%0.2f", d)
}

type database map[string]dollar

// func (db database) ServeHTTP(w http.ResponseWriter, req *http.Request) {
// 	for item, price := range db {
// 		fmt.Fprintf(w, "%s: %s\n", item, price)
// 	}
// }

// func (db database) ServeHTTP(w http.ResponseWriter, req *http.Request) {
// 	switch req.URL.Path {
// 	case "/list":
// 		for item, price := range db {
// 			fmt.Fprintf(w, "%s: %s\n", item, price)
// 		}
// 	case "/price":
// 		item := req.URL.Query().Get("item")
// 		price, ok := db[item]
// 		if !ok {
// 			w.WriteHeader(http.StatusNotFound)
// 			fmt.Fprintf(w, "no such item: %q\n", item)
// 		} else {
// 			fmt.Fprintf(w, "%s\n", price)
// 		}
// 	default:
// 		w.WriteHeader(http.StatusNotFound)
// 		fmt.Fprintf(w, "no such page %s\n", req.URL)
// 	}
// }

func (db database) list(w http.ResponseWriter, req *http.Request) {
	for item, price := range db {
		fmt.Fprintf(w, "%s: %s\n", item, price)
	}
}

func (db database) price(w http.ResponseWriter, req *http.Request) {
	item := req.URL.Query().Get("item")
	price, ok := db[item]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "No such item %s\n", item)
	} else {
		fmt.Fprintf(w, "%s\n", price)
	}
}
