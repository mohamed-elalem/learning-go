package main

import (
	"fmt"

	bank1 "./bank3"
)

func main() {
	// bank1.Init()
	bank1.Deposit(500)
	fmt.Println(bank1.Balance())
	bank1.Deposit(200)
	fmt.Println(bank1.Balance())
	bank1.Deposit(800)
	fmt.Println(bank1.Balance())
	bank1.Deposit(300)
	fmt.Println(bank1.Balance())
	bank1.Deposit(200)
	fmt.Println(bank1.Balance())
	bank1.Deposit(300)
	fmt.Println(bank1.Balance())
	bank1.WithDraw(600)
	fmt.Println(bank1.Balance())

}
