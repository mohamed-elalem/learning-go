package main

import (
	"fmt"

	"./urlvalues"
)

func main() {
	m := urlvalues.Values{"lang": {"en"}}
	m.Add("item", "1")
	m.Add("item", "2")

	fmt.Println(m.Get("lang"))
	fmt.Println(m.Get("q"))
	fmt.Println(m["item"])

	n := urlvalues.Values{}
	fmt.Println(n, m)

	m = nil
	fmt.Println(m.Get("item"))
	// m.Add("item", "3") panic
}
