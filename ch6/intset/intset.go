package intset

import (
	"bytes"
	"fmt"
)

type IntSet struct {
	words []uint64
}

func (s *IntSet) Has(x int) bool {
	word, bit := x/64, uint(x%64)
	return word < len(s.words) && (s.words[word]&(1<<bit)) != 0
}

func (s *IntSet) Add(x int) {
	word, bit := x/64, uint(x%64)
	for word >= len(s.words) {
		s.words = append(s.words, 0)
	}
	s.words[word] |= (1 << bit)
}

func (s *IntSet) UnionWith(t *IntSet) {
	for i, tword := range t.words {
		if i < len(s.words) {
			s.words[i] |= tword
		} else {
			s.words = append(s.words, tword)
		}
	}
}

func (s *IntSet) String() string {
	var buf bytes.Buffer

	buf.WriteByte('{')
	for i, word := range s.words {
		if word == 0 {
			continue
		}
		for j := 0; j < 64; j++ {
			if word&(1<<uint(j)) != 0 {
				if buf.Len() > len("{") {
					buf.WriteByte(' ')
				}
				fmt.Fprintf(&buf, "%d", 64*i+j)
			}
		}
	}

	buf.WriteByte('}')
	return buf.String()
}

func (s *IntSet) Len() int {
	cnt := 0
	for _, bits := range s.words {
		for bits > 0 {
			bits &^= (bits & -bits)
			cnt++
		}
	}
	return cnt
}

func (s *IntSet) Remove(x int) {
	word := x / 64
	if word < len(s.words) {
		s.words[word] &^= (1 << (uint(x) % 64))
	}
}

func (s *IntSet) Clear() {
	s.words = s.words[:0]
}

func (s *IntSet) Copy() (copy IntSet) {
	copy = *new(IntSet)
	copy.UnionWith(s)
	return
}

func (s *IntSet) AddAll(bits ...int) {
	for _, bit := range bits {
		s.Add(bit)
	}
}

func (s *IntSet) IntersectWith(t *IntSet) *IntSet {
	var smaller, bigger, intersection *IntSet = t, s, new(IntSet)

	if len(s.words) < len(t.words) {
		smaller = s
		bigger = t
	}

	for word, bits := range smaller.words {
		intersection.words = append(intersection.words, bits&bigger.words[word])
	}

	return intersection
}

func (s *IntSet) DifferenceWith(t *IntSet) *IntSet {
	unioned := s.Copy()
	unioned.UnionWith(t)
	difference := new(IntSet)
	for word, bits := range t.words {
		difference.words = append(difference.words, bits^unioned.words[word])
	}
	difference.words = append(difference.words, unioned.words[len(t.words):]...)
	return difference
}

func (s *IntSet) SymmetricDifferenceWith(t *IntSet) *IntSet {
	notInT := s.DifferenceWith(t)
	notInS := t.DifferenceWith(s)
	notInT.UnionWith(notInS)
	return notInT
}

func (s *IntSet) Elems() (iteratable []uint64) {
	for word, bits := range s.words {
		for i := 0; i < 64; i++ {
			if bits&(1<<uint(i)) > 0 {
				iteratable = append(iteratable, uint64(word*64+i))
			}
		}
	}
	return
}
