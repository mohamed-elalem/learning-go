package main

import (
	"fmt"

	"./intset"
)

func main() {
	var x, y intset.IntSet
	x.Add(1)
	x.Add(144)
	x.Add(9)
	fmt.Println(x.String())
	y.Add(9)
	y.Add(42)
	fmt.Println(y.String())
	x.UnionWith(&y)
	fmt.Println(&x)
	fmt.Println(x.Has(9), x.Has(123))
	fmt.Println(x.Len())
	x.Remove(9)
	fmt.Println(&x)
	y.Clear()
	fmt.Println(y.String())
	z := x.Copy()
	fmt.Println(&x, &z)
	z.AddAll(1, 62, 485, 236, 111, 458, 4652)
	fmt.Println(z.String())
	fmt.Println(z.IntersectWith(&x))
	fmt.Println(z.DifferenceWith(&x))
	x.AddAll(1, 5, 6, 9, 8, 5, 2, 3, 64, 12, 1, 21, 5, 4, 54, 8, 4)
	fmt.Println(x.SymmetricDifferenceWith(&z))

	fmt.Printf("%v\n", x.Elems())
}
