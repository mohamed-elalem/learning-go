package main

import (
	"fmt"
	"image/color"

	"./coloredpoint"
)

func main() {
	var cp coloredpoint.ColoredPoint
	cp.Point = new(coloredpoint.Point)
	cp.X = 1
	fmt.Println(cp.Point.X)
	cp.Point.Y = 2
	fmt.Println(cp.Y)

	red := color.RGBA{255, 0, 0, 255}
	blue := color.RGBA{0, 0, 255, 255}

	// p := coloredpoint.ColoredPoint{Point: coloredpoint.Point{1, 1}, Color: red}
	// q := coloredpoint.ColoredPoint{coloredpoint.Point{5, 4}, blue}
	p := coloredpoint.ColoredPoint{Point: &coloredpoint.Point{1, 1}, Color: red}
	q := coloredpoint.ColoredPoint{&coloredpoint.Point{5, 4}, blue}
	p.Point = q.Point
	p.ScaleBy(2)
	q.ScaleBy(2)
	fmt.Println(p.Distance(*q.Point))
	fmt.Println(*p.Point, *q.Point)
}
