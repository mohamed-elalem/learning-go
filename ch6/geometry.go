package main

import (
	"fmt"
	"math/rand"

	"./geometry"
)

func main() {
	x := geometry.Point{1, 2}
	y := geometry.Point{3, 4}

	fmt.Printf("%#v\n", x.Distance(y))

	path := geometry.Path{}

	for i := 0; i < 50; i++ {
		path = append(path, geometry.Point{rand.Float64(), rand.Float64()})
	}

	fmt.Printf("%v\n", path)

	fmt.Printf("%v\n", path.Distance())

	x.ScaleBy(5)
	// geometry.Point{5, 6}.ScaleBy(20) cannot call pointer method on geometry.Point literal
	z := &geometry.Point{1, 5}
	z.ScaleBy(20)
	fmt.Printf("%v\n", *z)
}
