package server

import (
	"log"
	"net"
)

func serve() {
	listener, err := net.Listen("ftp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
		return
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go handleConn(conn)
	}
}

func handleConn(conn net.Conn) {
	defer conn.Close()

}
