package main

import (
	"fmt"
)

func main() {
	naturals := make(chan int)
	squares := make(chan int)

	// go func() {
	// 	for x := 0; x < 100; x++ {
	// 		naturals <- x
	// 	}
	// 	close(naturals)
	// }()

	// go func() {
	// 	// for {
	// 	// x, ok := <-naturals
	// 	// if !ok {
	// 	// 	break
	// 	// }
	// 	// squares <- x * x
	// 	// }
	// 	for x := range naturals {
	// 		squares <- x * x
	// 	}
	// 	close(squares)
	// }()

	// // for {
	// // 	fmt.Println(<-squares)
	// // 	// time.Sleep(1 * time.Second)
	// // }
	// for x := range squares {
	// 	fmt.Println(x)
	// 	time.Sleep(50 * time.Millisecond)
	// }

	go counter(naturals)
	go squarer(squares, naturals)
	printer(squares)
}

func counter(out chan<- int) {
	fmt.Println("Counting...")
	for x := 0; x < 100; x++ {
		out <- x
	}
	close(out)
}

func squarer(out chan<- int, in <-chan int) {
	for x := range in {
		out <- x * x
	}
	close(out)
}

func printer(in <-chan int) {
	fmt.Println("Printing...")
	for x := range in {
		fmt.Println(x)
	}
}
