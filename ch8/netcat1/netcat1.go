package main

import (
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", strings.Join([]string{os.Args[1], os.Args[2]}, ":"))
	if err != nil {
		log.Fatal(err)
	}
	done := make(chan struct{})
	// defer func() {
	// 	conn.Close()
	// 	<-done
	// 	log.Println("Closed connection\nSignaling Channel to close")
	// }()
	// go func() {
	// 	mustCopy(os.Stdout, conn)
	// 	log.Println("done")
	// 	done <- struct{}{}
	// 	log.Println("Channel synchronized")
	// }()
	// mustCopy(conn, os.Stdin)
	go func() {
		io.Copy(os.Stdout, conn)
		log.Println("Closing connection")
		done <- struct{}{}
	}()
	mustCopy(conn, os.Stdin)
	if tcpConn, ok := conn.(*net.TCPConn); ok {
		log.Println("Ok Casted")
		tcpConn.CloseWrite()
	}
	log.Println("Method ended")
	<-done
	log.Println("Finally closed")
}

func mustCopy(dst io.Writer, src io.Reader) {
	if _, err := io.Copy(dst, src); err != nil {
		log.Print(err)
	}
}
