package main

import "fmt"

func main() {
	var myInt int = 5
	var myFloat float32 = 5.56
	x, y := 20, 6
	x, y = y, x

	fmt.Println(myInt, myFloat, x, y)

	var p = new(int)
	*p = 50
	fmt.Println(*p)

	cmp1 := complex(5, 4)
	cmp2 := 5 + 4i

	fmt.Println(cmp1, cmp2)
}
