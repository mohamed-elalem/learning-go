package main

import "fmt"
import "strings"

func main() {
	fmt.Println(simpleBasename("/home/mohamed/a.b.c.go"))
}

func basename(s string) string {
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '/' {
			s = s[i+1:]
			break
		}
	}
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '.' {
			s = s[:i]
			break
		}
	}
	return s
}

func simpleBasename(s string) string {
	s = s[strings.LastIndex(s, "/")+1:]
	if dot := strings.LastIndex(s, "."); dot >= 0 {
		s = s[:dot]
	}
	return s
}
