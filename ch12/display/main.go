package main

import (
	"fmt"
	"os"
	"reflect"

	"../sexpr"
	"./display"
)

type Movie struct {
	Title, Subtitle string
	Year            int
	Color           bool
	Actor           map[string]string
	Oscars          []string
	Sequel          *string
}

func main() {
	strangelove := Movie{
		Title:    "Dr. Strangelove",
		Subtitle: "How I Learned to Stop Worrying and Love the Bomb",
		Year:     1964,
		Color:    false,
		Actor: map[string]string{
			"Dr. Strangelove":            "Peter Sellers",
			"Grp. Capt. Lionel Mandrake": "Peter Sellers",
			"Pres. Merkin Muffley":       "Peter Sellers",
			"Gen. Buck Turgidson":        "George C. Scott",
			"Brig. Gen. Jack D. Ripper":  "Sterling Hayden",
			`Maj. T.J. "King" Kong`:      "Slim Pickens",
		},
		Oscars: []string{
			"Best Actor (Nomin.)",
			"Best Adapted Screenplay (Nomin.)",
			"Best Director (Nomin.)",
			"Best Picture (Nomin.)",
		},
	}

	display.Display("strangelove", strangelove)
	display.Display("os.Stderr", os.Stderr)
	display.Display("rv", reflect.ValueOf(os.Stderr))
	var i interface{} = 3
	display.Display("i", i)
	display.Display("i", &i)
	rv, _ := (sexpr.Marshal(strangelove))
	fmt.Println(string(rv[:]))
	var tmp *Movie = &Movie{}
	err := sexpr.Unmarshal(rv, tmp)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%#v\n", tmp)
	}
}
