package main

import (
	"fmt"
	"unsafe"
)

type X struct {
	a bool
	e bool
	b int16
	c []int
	d struct{}
}

func main() {
	x := X{a: false, e: true, b: 156, c: []int{}, d: struct{}{}}
	fmt.Println(unsafe.Sizeof(x), unsafe.Sizeof(x.a), unsafe.Sizeof(x.e), unsafe.Sizeof(x.b), unsafe.Sizeof(x.c), unsafe.Sizeof(x.d))
	fmt.Println(unsafe.Alignof(x), unsafe.Alignof(x.a), unsafe.Alignof(x.e), unsafe.Alignof(x.b), unsafe.Alignof(x.c), unsafe.Alignof(x.d))
	fmt.Println("?", unsafe.Offsetof(x.a), unsafe.Offsetof(x.e), unsafe.Offsetof(x.b), unsafe.Offsetof(x.c), unsafe.Offsetof(x.d))
	pb := (*int16)(unsafe.Pointer(uintptr(unsafe.Pointer(&x)) + unsafe.Offsetof(x.b)))
	*pb = 1258
	fmt.Println(x.b)
}
