package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	start := time.Now()
	s, sep := "", ""

	for index, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "
		fmt.Println(index, arg)
	}
	// fmt.Println(s)
	fmt.Println(time.Since(start))
}

// func main() {
// 	start := time.Now()
// 	fmt.Println(strings.Join(os.Args[1:], " "))
// 	fmt.Println(time.Since(start))
// }
