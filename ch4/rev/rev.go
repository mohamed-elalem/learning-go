package main

import "fmt"

func rev(s []int) {
	for i, j := 0, len(s)-1; i < len(s); i++ {
		s[i], s[j] = s[j], s[i]
	}
}

func main() {
	// slice
	s := []int{1, 2, 3, 4, 5, 6}
	fmt.Println(len(s), cap(s))
	rev(s[:2])
	rev(s[2:])
	rev(s[:])
	fmt.Println(s, len(s), cap(s))

	// array
	a := [...]int{1, 2, 3, 4, 5}
	fmt.Printf("%T %T\n", s, a)
}
