package main

import (
	"crypto/sha512"
	"fmt"
)

func main() {
	c1 := sha512.Sum512([]byte("x"))
	c2 := sha512.Sum512([]byte("x"))
	fmt.Printf("%X\n%x\n%t\n%[1]T\n", c1, c2, c1 == c2)
}
