package main

type Employee struct {
	id     int
	name   string
	salary float32
}

func getEmployee() *Employee {
	employee := new(Employee)
	employee.id = 5
	employee.name = "kk"
	employee.salary = 5000.00
	return employee
}

func main() {
	getEmployee().salary = 2000
}
