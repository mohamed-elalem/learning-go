package main

import "fmt"

type tree struct {
	value       int
	left, right *tree
}

func Sort(arr []int) {
	var root *tree

	for _, v := range arr {
		root = add(root, v)
	}
	appendValues(arr[:0], root)
}

func appendValues(values []int, t *tree) []int {
	if t != nil {
		values = appendValues(values, t.left)
		values = append(values, t.value)
		values = appendValues(values, t.right)
	}
	return values
}

func add(t *tree, value int) *tree {
	if t == nil {
		t = new(tree)
		t.value = value
	} else if value < t.value {
		t.left = add(t.left, value)
	} else {
		t.right = add(t.right, value)
	}

	return t
}

func main() {
	arr := []int{5, 4, 3, 8, 6, 4, 8, 6, 2, 3, 2, 1, 3}
	Sort(arr)
	fmt.Printf("%v\n", arr)
}
