package main

import (
	"fmt"
	htmltemplate "html/template"
	"log"
	"os"
	"text/template"
	"time"

	"./github"
)

const templ = `
	{{.TotalCount}} issues:
	{{range .Items}}----------------------------------
	Number: {{.Number}}
	User: {{.User.Login}}
	Title: {{.Title | printf "%0.64s"}}
	Age: {{.CreatedAt | daysAgo}} days
	{{end}}
`

const htmlTempl = `
	<h1>{{.TotalCount}} Issues</h1>
	<table>
		<tr style='text-align: left'>
			<th>#</th>
			<th>State</th>
			<th>User</th>
			<th>Title</th>
		</tr>
		{{range.Items}}

		<tr>
			<td><a href='{{.HTMLURL}}'>{{.Number}}</td>
			<td>{{.State}}</td>
			<td><a href='{{.User.HTMLURL}}'>{{.User.Login}}</a></td>
			<td><a href='{{.HTMLURL}}'>{{.Title}}</a></td>
			<td></td>
		</tr>
		{{end}}
	</table>
`

func daysAgo(t time.Time) int {
	return int(time.Since(t).Hours() / 24)
}

var report = template.Must(template.New("report").Funcs(template.FuncMap{"daysAgo": daysAgo}).Parse(templ))
var htmlreport = htmltemplate.Must(htmltemplate.New("htmlreport").Parse(htmlTempl))

func main() {
	result, err := github.SearchIssues(os.Args[1:])

	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%d issues:\n", result.TotalCount)
	for _, item := range result.Items {
		fmt.Printf("#%-5d %9.9s %0.55s\n", item.Number, item.User.Login, item.Title)
	}

	if err := report.Execute(os.Stdout, result); err != nil {
		log.Fatal(err)
	}
	htmlfile, _ := os.Create("htmlfile.html")
	if err := htmlreport.Execute(htmlfile, result); err != nil {
		log.Fatal(err)
	}
	htmlfile.Close()

}
