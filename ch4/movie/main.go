package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Movie struct {
	Title  string
	Year   int  `json:"released"`
	Color  bool `json:"color,omitempty"`
	Actors []string
}

var movies = []Movie{
	{Title: "Casablanca", Year: 1942, Color: false, Actors: []string{"Humphry Bogart", "Ingrid Bergman"}},
	{Title: "Cool Hand Luke", Year: 1967, Color: true, Actors: []string{"Paul Newman"}},
	{Title: "Bullit", Year: 1968, Color: true, Actors: []string{"Steve McQueen", "Jacqueline Bisset"}},
}

func main() {
	fmt.Println(movies)

	data, err := json.Marshal(movies)
	// data, err := json.MarshalIndent(movies, "", "    ")

	if err != nil {
		log.Fatalf("Json couldn't be formatted: %s\n", err)
	}
	fmt.Printf("%s\n", data)

	var titles []struct{ Title string }

	if err := json.Unmarshal(data, &titles); err != nil {
		log.Fatalf("Json Unmarshalling failed: %s\n", err)
	}

	fmt.Println(titles)

	var moviesResponse []Movie

	if err := json.Unmarshal(data, &moviesResponse); err != nil {
		log.Fatalf("Json Unmarshalling failed: %s\n", err)
	}

	fmt.Println(moviesResponse)
}
