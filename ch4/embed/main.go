package main

import "fmt"

type Point struct {
	X, Y int
}

type Circle struct {
	Point
	Radius int
}

type Wheel struct {
	Circle
	Spokes int
}

func main() {
	w := Wheel{Circle{Point{1, 2}, 3}, 4}
	fmt.Printf("%#v\n", w)
	w.Circle.Point.X = 6
	w.Y = 20
	fmt.Printf("%#v\n", w)
}
