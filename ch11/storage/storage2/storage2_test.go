package storage2

import (
	"strings"
	"testing"
)

func TestCheckQuotaNotifiesUser(t *testing.T) {
	saved := notifyUser
	defer func() { notifyUser = saved }()
	var notifiedUser, notifiedMsg string
	notifyUser = func(user, msg string) {
		t.Log(user, msg)
		notifiedUser, notifiedMsg = user, msg
	}

	const user = "joe@example.com"
	CheckQuota(user)
	if notifiedUser == "" && notifiedMsg == "" {
		t.Fatalf("Notifying isn't called, notifiedUser = %s, notifiedMsg = %s", notifiedUser, notifiedMsg)
	}

	if notifiedUser != user {
		t.Errorf("Wrong user (%s) notified, want %s", notifiedUser, user)
	}

	const wantSubstring = "98% of your quota"
	if !strings.Contains(notifiedMsg, wantSubstring) {
		t.Errorf("Unexpected notification msg <<%s>>, want substring %q", notifiedMsg, wantSubstring)
	}
}
